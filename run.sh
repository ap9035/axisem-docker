source /cvmfs/cvmfs.grid.sinica.edu.tw/hpc/mpi/scripst/openmpi-4.1.0_gcc4.8.5_centos7.sh
singularity exec /opt/hpc_soft/twgrid/axisem/axisem.sif cp /axisem . -R
cd axisem/MESHER
# mpirun singularity exec /opt/hpc_soft/twgrid/axisem/axisem.sif ./submit.csh
mpirun -np 32 -machinefile $MACHINES singularity exec /opt/hpc_soft/twgrid/axisem/axisem.sif ./submit.csh
